package com.nosirrom.feedafriend;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public final class FeedAFriend extends JavaPlugin implements Listener{

	@Override
	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
		//need to register this as a listener with bukkit otherwise the eventhandler breaks
	}
	
	@Override
	public void onDisable(){
		//don't really need this.
	}
	
	@EventHandler
	public void playerTouchEvent(PlayerInteractEntityEvent event){
		if(event.getRightClicked() instanceof Player){
			getLogger().info("Player has rightclicked on an instance of Player(friend)");
			Player friend = (Player) event.getRightClicked();
			Player player = event.getPlayer();
			getLogger().info("retrieved Player(friend) object");
			
			if(player.getItemInHand() != null && player.getItemInHand().getData() != null && player.getItemInHand().getType().isEdible()){
				//need to determine if player is holding anything, and if that item is food.
				
				if(friend.getFoodLevel() < 20){ // is this safe? 
					
					ItemStack itemstack = player.getItemInHand();
					FoodValues value = new FoodValues();
					friend.setFoodLevel(friend.getFoodLevel() + value.getItemHunger(itemstack.getType()));
					friend.setSaturation(friend.getSaturation() + value.getItemSaturation(itemstack.getType()));
					value.setFriendFoodEffect(friend, player, itemstack.getType());
					
					if(itemstack.getAmount() > 1){ //decrease by 1 if more than 1 item
						itemstack.setAmount(itemstack.getAmount() - 1); 
					} else if(itemstack.getAmount() == 1 || itemstack.getAmount() == 0){
						player.getInventory().remove(itemstack); //remove itemstack if only 1 item
						
						
					} else {
						getLogger().info("Error: could not decrease itemstack");
					}
					
					//particles and sound effects
					friend.getWorld().playSound(friend.getLocation(), Sound.EAT, 1, 1);
					friend.getWorld().playSound(friend.getLocation(), Sound.BURP, 1, 1);
					
					getLogger().info("feeding friend is successful and nothing crashed!");
					
				} else {
					getLogger().info("Friend is not hungry");
				}
			
			} else {
				getLogger().info("player is not holding food");
			}
			
		} else {
			getLogger().info("is not instance of player");
		}
	}

}
