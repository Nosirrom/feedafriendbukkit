package com.nosirrom.feedafriend;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class FoodValues {

	Random rand = new Random();
	// type, duration(ticks), amplifier
	PotionEffect flesh = new PotionEffect(PotionEffectType.HUNGER, 600, 0);
	PotionEffect lowpoison = new PotionEffect(PotionEffectType.POISON, 100, 0);
	PotionEffect pufferhunger = new PotionEffect(PotionEffectType.HUNGER, 300,
			2);
	PotionEffect puffernaseua = new PotionEffect(PotionEffectType.CONFUSION,
			300, 1);
	PotionEffect pufferpoison = new PotionEffect(PotionEffectType.POISON, 1200,
			3);
	PotionEffect appleregen = new PotionEffect(PotionEffectType.REGENERATION,
			100, 1);
	PotionEffect appleabsorp = new PotionEffect(PotionEffectType.ABSORPTION,
			2400, 0);
	PotionEffect eappleregen = new PotionEffect(PotionEffectType.REGENERATION,
			600, 0);
	PotionEffect eappledamageres = new PotionEffect(
			PotionEffectType.DAMAGE_RESISTANCE, 6000, 0);
	PotionEffect eapplefireres = new PotionEffect(
			PotionEffectType.FIRE_RESISTANCE, 6000, 0);


	public int getItemHunger(Material mat) {

		if (mat == Material.POTATO_ITEM) {
			return 1;
		}
		if (mat == Material.CAKE_BLOCK || mat == Material.COOKIE
				|| mat == Material.MELON || mat == Material.POISONOUS_POTATO
				|| mat == Material.RAW_CHICKEN || mat == Material.RAW_FISH
				|| mat == Material.SPIDER_EYE) {
			return 2;
		}
		if (mat == Material.PORK || mat == Material.RAW_BEEF) {
			return 3;
		}
		if (mat == Material.APPLE || mat == Material.CARROT_ITEM
				|| mat == Material.GOLDEN_APPLE || mat == Material.ROTTEN_FLESH) {
			return 4;
		}
		if (mat == Material.BREAD || mat == Material.COOKED_FISH) {
			return 5;
		}
		if (mat == Material.BAKED_POTATO || mat == Material.COOKED_CHICKEN
				|| mat == Material.GOLDEN_CARROT
				|| mat == Material.MUSHROOM_SOUP) {
			return 6;
		}
		if (mat == Material.COOKED_BEEF || mat == Material.GRILLED_PORK
				|| mat == Material.PUMPKIN_PIE) {
			return 8;
		}

		// error log here if I ever figure out how.

		return 0;
	}

	public float getItemSaturation(Material mat) {

		if (mat == Material.CAKE_BLOCK || mat == Material.COOKIE) {
			return 0.4F;
		}
		if (mat == Material.POTATO_ITEM) {
			return 0.6F;
		}
		if (mat == Material.ROTTEN_FLESH) {
			return 0.8F;
		}
		if (mat == Material.MELON || mat == Material.POISONOUS_POTATO
				|| mat == Material.RAW_CHICKEN || mat == Material.RAW_FISH) {
			return 1.2F;
		}
		if (mat == Material.PORK || mat == Material.RAW_BEEF) {
			return 1.8F;
		}
		if (mat == Material.APPLE) {
			return 2.4F;
		}
		if (mat == Material.SPIDER_EYE) {
			return 3.2F;
		}
		if (mat == Material.CARROT_ITEM || mat == Material.PUMPKIN_PIE) {
			return 4.8F;
		}
		if (mat == Material.BREAD || mat == Material.COOKED_FISH) {
			return 6F;
		}
		if (mat == Material.BAKED_POTATO || mat == Material.COOKED_CHICKEN
				|| mat == Material.MUSHROOM_SOUP) {
			return 7.2F;
		}
		if (mat == Material.GOLDEN_APPLE) {
			return 9.6F;
		}
		if (mat == Material.COOKED_BEEF || mat == Material.GRILLED_PORK) {
			return 12.8F;
		}
		if (mat == Material.GOLDEN_CARROT) {
			return 14.4F;
		}

		// error log here if I ever figure out how.

		return 0.0F;
	}

	public boolean setFriendFoodEffect(Player friend,Player player, Material mat) {

		if (mat == Material.ROTTEN_FLESH) { //30sec hunger1
			int num = rand.nextInt(100) + 1; //80% chance
			if(num < 81 && num > 0){
				friend.addPotionEffect(flesh);
			}
			return true;
		}
		if (mat == Material.SPIDER_EYE) { //5sec poison1
			friend.addPotionEffect(lowpoison);
			return true;
		}
		if (mat == Material.POISONOUS_POTATO) { //5sec poison1
			int num = rand.nextInt(100) + 1; //60% chance
			if(num < 61 && num > 0){
				friend.addPotionEffect(lowpoison);
			}
			return true;
		}
		if (mat == Material.RAW_FISH
				&& player.getItemInHand().getDurability() == 3) { // all fish have same id but different data value

			friend.addPotionEffect(pufferpoison);
			friend.addPotionEffect(pufferhunger);
			friend.addPotionEffect(puffernaseua);
			// hunger3 15sec, naseua2 15sec, poison4 60sec
			return true;
		}
		if (mat == Material.GOLDEN_APPLE
				&& player.getItemInHand().getDurability() == 0) {

			friend.addPotionEffect(appleregen);
			friend.addPotionEffect(appleabsorp);
			// regen2 5seconds, absorption1 2mins
			return true;
		}
		if (mat == Material.GOLDEN_APPLE
				&& player.getItemInHand().getDurability() == 1) {
			// THIS IS FOR ENCHANTED GOLDEN APPLE
			friend.addPotionEffect(eappledamageres);
			friend.addPotionEffect(eapplefireres);
			friend.addPotionEffect(eappleregen);
			friend.addPotionEffect(appleabsorp);
			return true;

		}
		return false;
	}

}
